<?php session_start();
include 'cookout.php';
include_once 'functions.php';
include_once 'objects.php';
if($_SESSION[auth] != 'yes'){
	include "spash_screen.php";die();
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Spacebook</title>
	<?php 	include 'linksAndScripts.php';
			include 'templates/floatingStars_JS.php';
	?>
</head>
<body>
	<?php include 'templates/floatingStars_HTML.php'; ?>

	
	<div id="page-wrap">
		<div class='container-fluid'>
			<div class='row-fluid'>
				<?php include "templates/spacebookHeader.php" ?>
			</div>
			<div style='margin-top:15px' class='row-fluid'>
				<div class='span3'>
					<?php include "templates/infoSidebar.php" ?>
				</div>
				<div class='span9'>
					<p class='planet-overview-header'>Planet Overview</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>