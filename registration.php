<?php session_start();error_reporting(E_ALL ^ E_NOTICE);
	ini_set('display_errors', 'On');include "objects.php";include_once "functions.php";
##Preprocessing##
	//Handling the actions in the registration process, based on $_POST[action]
	switch($_POST[action]){
		case "submitEmail":
			include 'data/emailcheck.inc';
			if(validEmail($_POST[email]) == true){
				if(checkEmail($_POST[email])){
					$_SESSION[regData][Email] = $_POST[email];
					$_POST[regStatus] = 1;
				}else{
					$_POST[regError] = "usedEmail";
				}
			}else{
				$_POST[regError] = "badEmail";
			}
			break;
		case "submitJobName":
			$_SESSION[regData]['Job Name'] = $_POST[jobName];
			$_POST[regStatus] = 2;
			break;
		case "submitJobDescription":
			$_SESSION[regData]['Job Description'] = $_POST[jobDesc];
			$_POST[regStatus] = 3;
			break;
		case "submitGoalJob":
			$_SESSION[regData]['Goal Job'] = $_POST[goalJob];
			$_SESSION[regData][goals] = array();
			$_POST[regStatus] = 4;
			break;
		case "submitGoal":
			array_push($_SESSION[regData][goals],$_POST[goal]);
			$num = count($_SESSION[regData][goals]);
			if($num < 5){
				$_POST[regStatus] = 4;
			}else{
				$_POST[regStatus] = 5;
				addUser($_SESSION[regData][Email],"New User",$_SESSION[regData][goals]);
			}

	}
	if($_POST[regStatus] == 4){
		if (count($_SESSION[regData][goals]) <= 4);
			}

 ?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Project Raptor | Registration</title>
		<?php include 'linksAndScripts.inc' ?>
	

</head>
<body>
	
	<div class='container-fluid'>
		<div class='hero-unit'>
			<div class='row-fluid'>
				<div class='span8'>
					<?php
						switch($_POST[regStatus]){
							case 1:
								include 'templates/registrationJobName.php';
								break;
							case 2:
								include 'templates/registrationJobDescription.php';
								break;
							case 3:
								include 'templates/registrationGoalJob.php';
								break;
							case 4:
								include "templates/registrationGoalList.php";
								break;
							case 5:
								include "templates/registrationComplete.php";
								break;
							default:
								include 'templates/registrationEmail.php';
						}
					?>
				</div>
				<div class='span4'>
					<?php if($_POST[regStatus] < 4 && $_POST[regStatus] > 0){include "templates/registrationInfoDisplay.php";}elseif($_POST[regStatus] == 4){include 'templates/registrationGoalListDisplay.php';} ?>
				</div>
		<script src='js/registration.js'></script>
</body>
</html>