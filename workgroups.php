<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 'On');
include "objects.php";
include_once "functions.php";

switch($_POST[action]){
	case "addWorkgroup":
		extract($_POST);
		createWorkgroup($name,$description,$_SESSION[UID]);
		break;
	case "addTask":
		extract($_POST);
		addWorkgroupTask($workgroupID,$name,$urgency,$importance,$comments,$userID);
		break;
	case "addTeam":
		extract($_POST);
		addTeam($name,$description,$workgroupID,$_SESSION[UID]);
		break;
}


?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Workgroups</title>
	<?php include 'linksAndScripts.inc'; ?>
</head>
<body>
	<?php include 'templates/navbar.php'; ?>
	<h1 class='center'>Workgroups</h1>
	<div class='container-fluid'>
		<div class='box-lifted span4'>
			<?php switch($_GET[action]){
				case "addWorkgroup":
					include "templates/addWorkgroup.php";
					break;
				default:
					include "templates/myWorkgroups.php";
					break;
				case "createTeam":
					include "templates/createTeam.php";
					break;
				case "showWorkgroup":
				case "addWorkgroupTask":
				case "addWorkgroupGoal":
				case "addWorkgroupEvent":
					include "templates/workgroupInfo.php";	
					break;


			} ?>
			
		</div>
		<div class='span8'>
			<?php 
				switch($_GET[action]){
					case "addWorkgroupTask":
						include "templates/addWorkgroupTask.php";
						break;
					case "addWorkgoupEvent":
						break;
					case "addWorkgroupGoal":
						break;
					case "showWorkgroup":
					case "addWorkgroupTask":
					case "addWorkgroupGoal":
					case "addWorkgroupEvent":
						include "templates/workgroupBlocks.php";
						break;
				}
				?>
		</div>
	</div>
</body>
</html>