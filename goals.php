<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 'On');
include "objects.php";
include_once "functions.php";

if($_POST[action] == "addGoal"){
	extract($_POST);
	addGoal($name,$date,$time,$comments);
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Goals</title>
	<?php include 'linksAndScripts.inc'; ?>
</head>
<body>
<?php include 'templates/navbar.php' ?>
<div class='container-fluid'>
	<div class='row-fluid'>
		<h1 class='center'>Goals</h1>
	</div>
	<div class='row-fluid'>
		<div class='span3'>
			<?php include "templates/addGoal.php" ?>
		</div>
		<div class='span9'>
			<?php include "templates/goalInfo.php";?>
		</div>
	</div>
</div>

<script>

</script>
</body>
</html>