<html>
<head>
	<title>Advising Panel</title>
	<?php include 'linksAndScripts.inc' ?>
</head>
<body style='padding-top:0'>
	<h1 class='FIN-bug center'>Gabby Gardiner</h1>
	<h2 class='center'>Financial Advisor</h2>
	<h3 class='center'>For Rickey Smith</h3>
	<div class='container-fluid'>
		<div class='row-fluid'>
			<div class='span6 box-lifted'>
				<h3>Tasks</h3>
				<table class='table sortable'><thead><tr><th>Task</th><th>Urgency</th><th>Importance</th><th>Action</th></tr></thead>
					<tbody>
						<tr><td>Go to the bank</td><td>50</td><td>60</td><td><a href="#">edit</a></td></tr>
						<tr><td>Deposit Paycheck</td><td>90</td><td>70</td><td><a href="#">edit</a></td></tr>
					</tbody>
				</table>
			</div>
			<div class='span6 box-lifted'>
				<h3>Events</h3>
				<table class='table sortable'><thead><tr><th>Event</th><th>Time Left</th><th>Action</th></tr></thead>
					<tbody>
						<tr><td>Payday!</td><td>4 days</td><td><a href='#'>Edit</a></td></tr>
						<tr><td>Payday!</td><td>3 Weeks</td><td><a href='#'>Edit</a></td></tr>
					</tbody>
				</table>
			</div>
</body>
</html>