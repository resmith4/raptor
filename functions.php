<?php
include 'cookout.php';
include_once 'objects.php';

$ship_classes = array(
"Apis",
"Culicidae",
"Vespula",
"Vespa",
"Hymenoptera",
"Lupus" ,
"Jamaicensis" ,
"Serpentes" ,
"Mississippiensis" ,
"Tigris" ,
"Leo" ,
"Gorilla" ,
"Capensis" ,
"Capra" ,
"Asinus" ,
"Caballus" ,
"Capra-P",
"Primigenius" ,
"Caballus-P",
"Elephantidae" ,
"Tarandus",
"Tarandus-P",
"Macrorhynchos" ,
"Livia" ,
"Africanus" ,
"Strigiformes" ,
"Latrans" ,
"Maritimus" ,
"Macrorhynchos-S"
);

$ships_fighters = array(
	"Apis",
	"Culicidae",
	"Vespula",
	"Vespa",
	"Hymenoptera"
	);

$ships_lightCruisers = array(
	"Lupus" ,
	"Jamaicensis" ,
	"Serpentes" ,
	"Mississippiensis" 
	);

$ships_heavyCruisers = array(
	"Tigris" ,
	"Leo" ,
	"Gorilla" 
	);

$ships_dreadnaughts = array(
	"Capensis"
	);

$ships_transport = array(
	"Capra" ,
	"Asinus" ,
	"Caballus" ,
	"Capra-P",
	"Primigenius" ,
	"Caballus-P",
	"Elephantidae" ,
	"Tarandus"
	);

$ships_special = array(
	"Macrorhynchos" ,
	"Livia" ,
	"Africanus" ,
	"Strigiformes" ,
	"Latrans" ,
	"Maritimus" ,
	"Macrorhynchos-S"
	);

function get_planet_fleet_ships($planetID){
	include "cookout.php";
	$getPlanetInfo = $db->prepare("SELECT * FROM `planets` WHERE `ID` = ?");
	$getPlanetInfo->execute(array($planetID));
	$row = $getPlanetInfo->fetch(PDO::FETCH_ASSOC);
	return $row;
}

function set_active_planet($planetID){
	include 'cookout.php';
	$checkOwnership = $db->prepare("SELECT * FROM `planets` WHERE `colonizedBy` = ?");
	$checkOwnership->execute(array($_SESSION[UID]));
	$num = $checkOwnership->rowCount();
	if($num > 0){
		//planet is owned by the logged in user
		$_SESSION[activePlanet] = $planetID;
	}else{
		echo "Error setting active planet. Planet not owned by active user";die();
	}
}

function set_first_user_planet(){
	include 'cookout.php';
	$getFirst = $db->prepare("SELECT `ID` FROM `planets` WHERE `colonizedBy` = ? LIMIT 1");
	$getFirst->execute(array($_SESSION[UID]));
	$row = $getFirst->fetch(PDO::FETCH_ASSOC);
	set_active_planet($row[ID]);
}

function login_user($username,$password){
	include 'cookout.php';
	$getUser = $db->prepare("SELECT * FROM `users` WHERE `username` = ?");
	$getUser->execute(array($username))or die(var_dump($getUser->errorInfo()));
	$num = $getUser->rowCount();
	if($num > 0){
		//username found
		$row = $getUser->fetch(PDO::FETCH_ASSOC);
		if($row[password] == md5($password)){
			//password matches
			##Setting The Info Variables ##
			$_SESSION[auth] = 'yes';
			$_SESSION[UID] = $row[ID];
			$_SESSION[username] = $row[username];
			##Setting the active planet to the first colinized planet##
			set_first_user_planet();

		}else{
			return "Username or Password not found";
		}

	}else{
		//username not found
		return "Username or Password not found";
	}
}

function getCredits($userID){
	include 'cookout.php';
	$getCredits = $db->prepare("SELECT `credits` FROM `users` WHERE `ID` = ?");
	$getCredits->execute(array($userID)) or die(var_dump($getCredits->errorInfo()));
	$row = $getCredits->fetch(PDO::FETCH_ASSOC);
	return $row[credits];
}

function getTributeSpins($userID){
	include 'cookout.php';
	$getTributeSpins = $db->prepare("SELECT `tributeSpins` FROM `users` WHERE `ID` = ?");
	$getTributeSpins->execute(array($userID)) or die(var_dump($getTributeSpins->errorInfo()));
	$row = $getTributeSpins->fetch(PDO::FETCH_ASSOC);
	return $row[tributeSpins];
}

function getUserPlanets($userID){
	include 'cookout.php';
	$getPlanets = $db->prepare("SELECT * FROM `planets` WHERE `colonizedBy` = ?");
	$getPlanets->execute(array($userID)) or die(var_dump($db->errorInfo()));
	$final = array();
	while($row = $getPlanets->fetch(PDO::FETCH_ASSOC)){
		array_push($final,$row);
	}

	return $final;
}

function getPlanetResourceArray($planetID){
	include 'cookout.php';
	$getPlanetInfo = $db->prepare("SELECT * FROM `planets` WHERE `ID` = ?");
	$getPlanetInfo->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$row = $getPlanetInfo->fetch(PDO::FETCH_ASSOC);
	$final = array(
		'ore'=> $row[available_Ore],
		'crystal' => $row[available_Crystal],
		'hydrogen' => $row[available_Hydrogen],
		'antimatter' => $row[available_Antimatter],
		'energyAvailable' => $row[available_Energy]);
	return $final;
};

function getEmpireName($userID){
	include 'cookout.php';
	$getEmpireName = $db->prepare("SELECT * FROM `users` WHERE `ID` = ?");
	$getEmpireName->execute(array($userID));
	$row = $getEmpireName->fetch(PDO::FETCH_ASSOC);
	return $row[empireName];
}

function getPlanetName($planetID){
	include 'cookout.php';
	$getEmpireName = $db->prepare("SELECT * FROM `planets` WHERE `ID` = ?");
	$getEmpireName->execute(array($planetID));
	$row = $getEmpireName->fetch(PDO::FETCH_ASSOC);
	return $row[name];

}

function getPlanetFields($planetID){
	include 'cookout.php';
	$getFields = $db->prepare("SELECT `total_fields`,`available_fields` FROM `planets` WHERE `ID` = ?");
	$getFields->execute(array($planetID));
	$row = $getFields->fetch(PDO::FETCH_ASSOC);
	$usedFields = $row[total_fields] - $row[available_fields];
	//$usedFields = "$usedFields";
	return "$usedFields/$row[total_fields]";
}

function getFleetInfo($userID){
	include 'cookout.php';
	$getFleets = $db->prepare("SELECT `total_fleets`,`available_fleets` FROM `users` WHERE `ID` = ?");
	$getFleets->execute(array($userID));
	$row = $getFleets->fetch(PDO::FETCH_ASSOC);
	$usedFleets = $row[total_fleets] - $row[available_fleets];
	return "$usedFleets/$row[total_fleets]";
}

function getProductionInfo($planetID){
	include 'cookout.php';
	$getProduction = $db->prepare("SELECT * FROM `planets` WHERE `ID` = ?");
	$getProduction->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$row = $getProduction->fetch(PDO::FETCH_ASSOC);
	$energy = $row[available_Energy] - $row[energy_use];
	$energy = "$energy";
	$final = array(
		"ore"=>$row[ore_production],
		"crystal"=>$row[crystal_production],
		"hydrogen"=>$row[hydrogen_production],
		"antimatter"=>$row[antimatter_production],
		"energyUse"=>$energy);
	return $final;
}

function echoAvailableResources($planetID){
	include 'cookout.php';
	$getResourceInfo = $db->prepare("SELECT `available_Ore`,`available_Crystal`,`available_Hydrogen` FROM `planets` WHERE `ID` = ?");
	$getResourceInfo->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	while($row = $getResourceInfo->fetch(PDO::FETCH_ASSOC)){
		echo "\n";
		foreach($row as $key=>$value){
			echo "<input type='hidden' id='$key' value='$value'>\n";
		}
	}
	return $row;	
}

function echoPlanetLocation($planetID){
	include 'cookout.php';
	$getLocation = $db->prepare("SELECT `location_galaxy`,`location_system`,`location_planet` FROM `planets` WHERE `ID` = ?");
	$getLocation->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$row = $getLocation->fetch(PDO::FETCH_ASSOC);
	echo "<input type='hidden' name='active_planet_location' id='active_planet_location' value='$row[location_galaxy]:$row[location_planet]:$row[location_planet]'>";
}

function getFleetSpeed($ships,$distance){
	$shipSpeeds = array();
	foreach($ships as $value){
		if($value[1] > 0){
			include 'cookout.php';
			$getShipSpeed = $db->prepare("SELECT * FROM `ship_classes` WHERE `code_name` = ?");
			$getShipSpeed->execute(array($value[0])) or die(var_dump($db->errorInfo()));
			$row = $getShipSpeed->fetch(PDO::FETCH_ASSOC);
			array_push($shipSpeeds,$row[base_speed]);
		}
	}
	sort($shipSpeeds);
	$travelTime = ($distance * 100) / $shipSpeeds[0];
	return($travelTime);
}

function getFleetFuelConsumption($ships,$distance){
	$fuelConsumption = 0;
	foreach($ships as $value){
		include 'cookout.php';
		$getShipSpeed = $db->prepare("SELECT * FROM `ship_classes` WHERE `code_name` = ?");
		$getShipSpeed->execute(array($value[0])) or die(var_dump($db->errorInfo()));
		$row = $getShipSpeed->fetch(PDO::FETCH_ASSOC);
		$fuelConsumption += ($row[base_fuel_consumption] * $value[1] * $distance)/100;
		$fuelConsumption = floor($fuelConsumption);
	}
	return($fuelConsumption);
}

function getFleetCargoSpace($ships){
	$cargoSpace = 0;
	foreach($ships as $value){
		include 'cookout.php';
		$getShipCargoCapacity = $db->prepare("SELECT `base_cargo_capacity` FROM `ship_classes` WHERE `code_name` = ? limit 1");
		$getShipCargoCapacity->execute(array($value[0])) or die(var_dump($db->errorInfo()));
		$row = $getShipCargoCapacity->fetch(PDO::FETCH_ASSOC);
		if($row[base_cargo_capacity] > 20){
			$cargoSpace += ($row[base_cargo_capacity] * $value[1]);
		}
	}
	return($cargoSpace);
}

function calculateDistance($start,$end){
	$origin = explode(":",$start);
	$target = explode(":",$end);
	//$array[0] = galaxy, $array[1] = planet, $array[2] = system

	if($origin[0] == $target[0]){
		//target is in the same galaxy
		if($origin[1] == $target[1]){
			//target is in the same system
			if($origin[2] == $target[2]){
				//target is the same planet as the origin. Weird. but whatever.
				$distance = 1;
			}else{
				//target is different planet in same system
				$distance = abs($origin[2] - $target[2]) * 10;
			}
		}else{
			//target is a different system in the same galaxy
			$distance = abs($origin[1] - $target[2]) * 1000;
		}
	}else{
		//target is a different galaxy
		$distance = abs($origin[0] - $target[1]) * 1000000;
	}
	return $distance;
}

function getPlanetLocationString($planetID){
	include 'cookout.php';
	$getLocation = $db->prepare("SELECT `location_galaxy`,`location_system`,`location_planet` FROM `planets` WHERE `ID` = ?");
	$getLocation->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$row = $getLocation->fetch(PDO::FETCH_ASSOC);
	$outputString = $row[location_galaxy].":".$row[location_system].":".$row[location_planet];
	return $outputString;
}

function buildShipsArrayFromPost(){
	$ships = array();
	foreach($_POST as $key => $value){
		if(substr($key,0,6) == "ships_"){
			$info = array($key,$value);
			array_push($ships,$info);
		}
	}
	return $ships;
}

function fuelCheck($fuelUse){
	include 'cookout.php';
	$getFuel = $db->prepare("SELECT `available_Hydrogen` FROM `planets` WHERE `ID` = $_SESSION[activePlanet]");
	$getFuel->execute(array());
	$row = $getFuel->fetch(PDO::FETCH_ASSOC) or die(var_dump($db->errorInfo()));
	if($row[available_Hydrogen] < $fuelUse){
		//not enough fuel on planet
		return "no";
	}else{
		//enough fuel on planet
		return "yes";
	}
}

function shipNumberCheck($ships){
	include 'cookout.php';
	foreach($ships as $value){
		$getNumberOfShipsParked = $db->prepare("SELECT `$value[0]` FROM `planets` WHERE `ID` = $_SESSION[activePlanet]");
		$getNumberOfShipsParked->execute(array()) or die(var_dump($db->errorInfo()));
		$row = $getNumberOfShipsParked->fetch(PDO::FETCH_ASSOC);
		if($row[$value[0]] < $value[1]){
			return "no";
		}
	}
	return "yes";
}

function cargoSpacecheck($ships,$resourcesLoaded){
	$cargoSpaceAvailable = getFleetCargoSpace($ships);
	if($resourcesLoaded[total] == 0){
		return "yes";
	}
	if($cargoSpaceAvailable > $resourcesLoaded[total]){
		return "yes";
	}else{
		return "no";
	}
}

function dispatchFleet(){
	include 'cookout.php';
	//exiting the function if no orders were selected
	if($_POST[fleetOrders] == "None"){
		$message = array("type"=>"danger","message"=>"No orders were given. Fleet not dispatched.");
		return $message;
	}
	$ships = buildShipsArrayFromPost();
	extract($_POST);
	//building Info
	$targetString = $_POST["destination-galaxy"].":".$_POST["destination-system"].":".$_POST["destination-planet"];
	$originString = getPlanetLocationString($_SESSION[activePlanet]);
	$distance = calculateDistance($originString,$targetString);
	$fuelUse = getFleetFuelConsumption($ships,$distance);
	$fleetTravelTime = getFleetSpeed($ships,$distance);
	$targetPlanetID = getPlanetIDFromArray(array($_POST["destination-galaxy"],$_POST["destination-system"],$_POST["destination-planet"]));
	//check to make sure there is enough fuel available on the planet to launch the fleet
	$enoughFuel = fuelCheck($fuelUse);
	if($enoughFuel == "no"){
		$message = array("type"=>"danger","message"=>"There is not enough hydrogen on your planet to launch your fleet.");
		return $message;
	}
	//check to make sure that the necessary ships are parked at the planet
	$enoughShips = shipNumberCheck($ships);
	if($enoughShips == "no"){
		$message = array("type"=>"danger","message"=>"You do not have enough ships parked at your planet to launch your fleet");
		return $message;
	}

	$resourcesLoaded = array(
		"ore"=>$_POST["ore-sent-input"],
		"crystal"=>$_POST["crystal-sent-input"],
		"hydrogen"=>$_POST["hydrogen-sent-input"],
		"total"=>$_POST["ore-sent-input"] + $_POST["crystal-sent-input"] + $_POST["hydrogen-sent-input"]
		);
	$enoughCargoSpace = cargoSpaceCheck($ships,$resourcesLoaded);
	if($enoughCargoSpace == "no"){
		$message = array("type"=>"danger","message"=>"There is not enough cargo space available to transport that amount of resources. Fleet not dispatched.");
		return $message;
	}

	//create the query
	$queries = array();

	$shiplistName = "";
	$shipListNumber = "";
	$shipListRemoveParked = "";

	$fleetTravelMinutes = floor($fleetTravelTime);

	foreach($ships as $key=>$value){
		if($value[1] > 0){
			$shiplistName = $shiplistName.",`$value[0]`";
			$shiplistNumber = $shiplistNumber.",$value[1]";
			if($shipListRemoveParked == ""){
				$shipListRemoveParked = $shipListRemoveParked."`$value[0]` = `$value[0]` - $value[1]";
			}else{
				$shipListRemoveParked = $shipListRemoveParked.",`$value[0]` = `$value[0]` - $value[1]";
			}
			
		}
	}
	$returnTripMinutes = $fleetTravelMinutes*2;

	$queries[0] = "INSERT INTO `fleets` (`userID`,`originPlanetID`,`targetPlanetID`,`type`,`arrival`".$shiplistName.") VALUES ($_SESSION[UID],$_SESSION[activePlanet],$targetPlanetID,'$_POST[fleetOrders]',NOW() + INTERVAL  $fleetTravelMinutes MINUTE".$shiplistNumber.")";
	$queries[1] = "INSERT INTO `fleets` (`userID`,`originPlanetID`,`targetPlanetID`,`type`,`arrival`,`returnTripOf`".$shiplistName.") VALUES ($_SESSION[UID],$_SESSION[activePlanet],$targetPlanetID,'Return',NOW() + INTERVAL $returnTripMinutes MINUTE,?".$shiplistNumber.")";
	$queries[2] = "UPDATE `planets` SET ".$shipListRemoveParked." WHERE `ID` = $_SESSION[activePlanet]";

	include 'cookout.php';
	$firstTrip = $db->prepare($queries[0]);
	$returnTrip = $db->prepare($queries[1]);
	$removeShips = $db->prepare($queries[2]);
	$removeShips->execute(array()) or var_dump($db->errorInfo());
	$firstTrip->execute(array()) or var_dump($db->errorInfo());
	$firstTripID = $db->lastInsertId();

	if($_POST[fleetOrders] != "Deploy"){
		$returnTrip->execute(array($firstTripID)) or die(var_dump($db->errorInfo()));		
	}




	$message = array("type"=>"success","message"=>"Fleet Dispatched.");
	return $message;
}

function updatePlanetResources($planetID){
	include 'cookout.php';
	$getTimeAndResources = $db->prepare("SELECT `last_update`,`ore_production`,`crystal_production`,`hydrogen_production` FROM `planets` WHERE `ID` = ?");
	$getTimeAndResources->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$row = $getTimeAndResources->fetch(PDO::FETCH_ASSOC);
	$previousLook = $row[last_update];
	$currentLook = time();
	//$currentLook = strtotime($currentLook);
	$previousLook = strtotime($previousLook);
	
	//the time between the two looks, in minutes.
	$timeBetweenLooks = ($currentLook - $previousLook) / 60;

	$oreProduced = ($row[ore_production] / 60) * $timeBetweenLooks;
	$crystalProduced = ($row[crystal_production] / 60) * $timeBetweenLooks;
	$hydrogenProduced = ($row[hydrogen_production] / 60) * $timeBetweenLooks;

	$updateResources = $db->prepare("UPDATE `planets` SET `available_Ore` = `available_Ore` + ?, `available_Crystal` = `available_Crystal` + ?, `available_Hydrogen` = `available_Hydrogen` + ?, `last_update` = NOW() WHERE `ID` = ?");
	$updateResources->execute(array($oreProduced,$crystalProduced,$hydrogenProduced,$planetID)) or die(var_dump($db->errorInfo()));

}

function getPlanetIDFromArray($array){
	include 'cookout.php';
	$getID = $db->prepare("SELECT `ID` FROM `planets` WHERE `location_galaxy` = ? AND `location_system` = ? AND `location_planet` = ?");
	$getID->execute($array);
	$row = $getID->fetch(PDO::FETCH_ASSOC);
	return $row[ID];
}

function secondsLeftIntoTime($seconds){
	if($seconds < 60){
		//time is less than a minute
		$seconds = "0".$seconds;
		$seconds = substr($seconds,-2);
		return "00:00:".$seconds;
	}else{
		if($seconds < (60*60)){
			//time is less than an hour
			$minutes = floor($seconds/60);
			$seconds = floor($seconds%60);
			$minutes = "0".$minutes;
			$seconds = "0".$seconds;
			$minutes = substr($minutes,-2);
			$seconds = substr($seconds,-2);
			return "00:".$minutes.":".$seconds;
		}else{
			//time is more than an hour
			$hours = floor($seconds/(60*60));
			$seconds = $seconds - ($hours * (60 * 60));
			$minutes = floor($seconds/60);
			$seconds = floor($seconds%60);

			$hours = "0".$hours;
			$minutes = "0".$minutes;
			$seconds = "0".$seconds;
			$hours = substr($hours,-2);
			$minutes = substr($minutes, -2);
			$seconds = substr($seconds, -2);
			return $hours.":".$minutes.":".$seconds;
		}
	}
}

function generateShipsFromFleetRow($row){
	$output = array();
	foreach($row as $key=>$value){
		if(substr($key,0,6) == "ships_"){
			if($value > 0){
				$input = array($key,$value);
				array_push($output,$input);				
			}
		}
	}
	return $output;
}

function processFleetArrival(){

}

function cancelFleet($fleetID){
	include 'cookout.php';
	$getFleetInfo = $db->prepare("SELECT * FROM `fleets` WHERE `ID` = ?");
	$getFleetInfo->execute(array($fleetID));
	$row = $getFleetInfo->fetch(PDO::FETCH_ASSOC);
	$now = time();
	$fleetTravelTime = $now - strtotime($row[dispatch]);

	$minutes_cancel = ceil($fleetTravelTime/60);

	$shipNameList_cancelFleet = "";

	foreach($row as $key=>$value){
		if(substr($key,0,6) == "ships_"){
			$shipNameList_cancelFleet = $shipNameList_cancelFleet.",`$key`";
			$shipNumberList_cancelFleet = $shipNumberList_cancelFleet.",$value";
		}
	}
	$insertString = "INSERT INTO `fleets` (`userID`,`originPlanetID`,`targetPlanetID`,`type`,`dispatch`,`arrival`,`returnTripOf`".$shipNameList_cancelFleet.") VALUES (".$row[userID].",".$row[originPlanetID].",".$row[targetPlanetID].",'Return','".$row[dispatch]."',NOW() + INTERVAL $minutes_cancel MINUTE,NULL".$shipNumberList_cancelFleet.")";
	
	$setReturnFleet = $db->prepare($insertString);
	$setReturnFleet->execute(array());

	$deletePrimary = $db->prepare("DELETE FROM `fleets` WHERE `ID` = ?");
	$deleteReturn = $db->prepare("DELETE FROM `fleets` WHERE `returnTripOf` = ?");

	$deletePrimary->execute(array($row[ID]));
	$deleteReturn->execute(array($row[ID]));
}