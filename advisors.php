<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 'On');
include "objects.php";
include_once "functions.php";
?>
<html>
<head>
	<title>Advisors</title>
	<?php include 'linksAndScripts.inc'; ?>
</head>
<body>
	<?php include 'templates/navbar.php' ?>
	<div class='container-fluid'>
		<div class='row-fluid'>
			<h1 class='center'>Advisors</h1>
		</div>
		<div class='row-fluid'>
			<div class='span3 advisorPanel ENV-bug'>
				<h4>Enviroment</h4>
				<p>-None-</p>
			</div>
			<div class='span3 advisorPanel CAR-bug'>
				<h4>Business/Career</h4>
				<p>-None-</p>
			</div>
			<div class='span3 advisorPanel FIN-bug'>
				<h4>Finances</h4>
				<p><a href='advisorPanelDemo.php'>Gabby Gardiner</a></p>
			</div>
			<div class='span3 advisorPanel HEA-bug'>
				<h4>Health</h4>
				<p>-None-</p>
			</div>
		</div>
		<div class='row-fluid'>
			<div class='span3 advisorPanel PPL-bug'>
				<h4>Family And Friends</h4>
				<p>-None-</p>
			</div>
			<div class='span3 advisorPanel ROM-bug'>
				<h4>Romance</h4>
				<p>-None-</p>
			</div>
			<div class='span3 advisorPanel GRO-bug'>
				<h4>Personal Growth</h4>
				<p>-None-</p>
			</div>
			<div class='span3 advisorPanel REC-bug'>
				<h4>Fun and Recreation</h4>
				<p>-None-</p>
			</div>
		</div>
	</div>
</body>
</html>