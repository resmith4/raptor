<!DOCTYPE HTML>
<html>
<head>
	<title>Project Raptor</title>
	<?php
		include 'linksAndScripts.inc';
	?>
</head>
<body style='background:black'>
	<div class='hero-unit'>
		<div class='row-fluid'>
			<div class='span6'>
		<h1>Wolfstacks</h1>
		<h2>Project Raptor</h2>
		<p>A new social time-management application</p>
		<a href='registration.php' class='btn btn-large btn-primary'>Sign Up <i class='icon-double-angle-right'></i></a>
	</div>
	<div class='span6' id='dialogTarget'>
		<div id='loginDialog'>
			<?php include "templates/loginForm.php" ?>
		</div>
	</div>
</div>
	</div>

	<script src="js/raptor.js"></script>
</body>
</html>