<?php
include 'cookout.php';

$techs = array(
	'Space',
	'Construction',
	"Communication",
	"Research",
	"Power",
	"Mining",
	"Fabrication",
	"Shipbuilding",
	"Rocketry",
	"Defense",
	"Robot",
	"Lasers",
	"Nuclear",
	"Pulse Drive",
	"Fusion",
	"Scanning",
	"Commerce",
	"Trade",
	"Missile",
	"Particle",
	"Warp Drive");
$ships=array(
"Apis Class",
"Culicidae Class",
"Vespula Class",
"Vespa Class",
"Hymenoptera Class",
"Lupus Class",
"Jamaicensis Class",
"Serpentes Class",
"Mississippiensis Class",
"Tigris Class",
"Leo Class",
"Gorilla Class",
"Capensis Class",
"Capra Class",
"Asinus Class",
"Caballus Class",
"Capra-P Variant",
"Primigenius Class",
"Caballus-P Variant",
"Elephantidae Class",
"Tarandus",
"Tarandus-P Variant",
"Macrorhynchos Class",
"Livia Class",
"Africanus Class",
"Strigiformes Class",
"Latrans Class",
"Maritimus Class",
"Macrorhynchos-S Variant"
);

$buildings=array(
	"Ore Power Plant",
	"Ore Power Plant (Power Module)",
	"Ore Power Plant (Efficiency Module)",
	"Ore Mine",
	"Ore Mine (Drill Module)",
	"Ore Mine (Power Conduit)",
	"Crystal Mine",
	"Crystal Mine (Drill Module)",
	"Crystal Mine (Power Conduit)",
	"Hydrogen Factory",
	"Hydrogen Factory (Production Module)",
	"Hydrogen Factory (Power Conduit)",
	"Base",
	"Bade (Admin Module)",
	"Base (Finance Department)",
	"Base (Trade Bureau)",
	"Research Lab",
	"Research Lab (Admin Module)",
	"Research Lab (Research Bay)",
	"Research Lab (Comm Array)",
	"Shipyard",
	"Shipyard (Fab Module)",
	"Shipyard (Construction Array)"
	);


function getChildren($array,$ID){
	$array_return = array();
	$array_children = array();
	include 'cookout.php';
	$getChildren = $db->prepare("SELECT * FROM `gameTechnlogyRequirements` WHERE `currentTech` = ?");
	$getInfo = $db->prepare("SELECT * FROM `gameTechnology` WHERE `ID` = ?");
	$getChildren->execute(array($ID)) or die("getChildren");
	while($row2 = $getChildren->fetch(PDO::FETCH_ASSOC)){
		$getInfo->execute(array($row2[leadsTo])) or die ("getInfo");
		while($row3 = $getInfo->fetch(PDO::FETCH_ASSOC)){
			$children = getChildren($children,$row3[ID]);
			if($children == null){
				$array = array(
				"name"=>$row3[name]);
				array_push($array_return,$array);
			}else{
				$array = array(
				"name"=>$row3[name],
				"children"=>$children);
				array_push($array_return,$array);
			}
		}
	}
	$count = count($array_return);
	if($count > 0){
		return $array_return;
	}else{return null;}
}


if($_GET[action] == "generateJson"){
	$final = array();
	include "cookout.php";
	$getAll = $db->prepare("SELECT * FROM `gameTechnology`");
	$baseCheck = $db->prepare("SELECT * FROM `gameTechnlogyRequirements` WHERE `leadsTo` = ?");
	$getAll->execute(array()) or die("getAll");
	while($row = $getAll->fetch(PDO::FETCH_ASSOC)){
		$baseCheck->execute(array($row[ID])) or die("basecheck");

		$num = $baseCheck->rowCount();
				//echo "<p>$row[ID] : $num</p>";
		if($num == 0){
			//echo "<p><strong>$row[ID]</strong></p>";
			$children = getChildren($children,$row[ID]);
			if($children == null){
				$array = array(
				"name"=>$row[name]);
				array_push($final,$array);
			}else{
				$array = array(
				"name"=>$row[name],
				"children"=>$children);
				array_push($final,$array);
			}
			
		}
	}

	echo json_encode($final);
}






if($_POST[action] == 'Init'){
	$db->query("Truncate table `gameTechnology`") or die("trun 1");
	$db->query("Truncate table `gameTechnlogyRequirements`")or die("trun 2");
	$insertTech = $db->prepare("INSERT INTO `gameTechnology` (`name`,`type`) VALUES (?,?)");
	$insertRequirement = $db->prepare("INSERT INTO `gameTechnlogyRequirements` (`currentTech`,`leadsTo`) VALUES (?,?)");

	foreach($techs as $value){
		$techCodes = array("I","II","III","IV","V","VI","VII","VIII","IX","X");
		for($x=0;$x<10;$x++){

			$name=$value." Tech ".$techCodes[$x];	
			$insertTech->execute(array($name,"tech")) or die(var_dump($db->errorInfo()));
			if($x<9){
				$id = $db->lastInsertId();
				$insertRequirement->execute(array($id,($id+1))) or die("failed inserting requirement");

			}
		}

	}

	foreach($ships as $value){

		$insertTech->execute(array($value,'ship')) or die("ships");
	}

	foreach($buildings as $value){

			$insertTech->execute(array($value,'building')) or die("buildings");
	}


}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Tech Dashboard</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
	<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
	<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>

</head>
<body>
<h1>Tech Dashboard</h1>

<?php 
if ($_POST[action] == "Init"){echo "<h2>Init Ran</h2>";} ?>
<div class='container-fluid'>
	<div class='row-fluid'>
		<form onkeypress="return event.keyCode != 13;" action='tech_dashboard.php' method='post'>
			<input type='submit' name='action' onclick="return confirm('Are you sure?')" value='Init' class='btn btn-primary'>
		</form>
		<a href='tech_dashboard.php?action=generateJson' class='btn btn-large btn-primary'>Generate JSON</a>
		<h2>Find Tech</h2>
		<form onkeypress="return event.keyCode != 13;" id='findTechForm' action='tech_dashboard.php' method='post'>
			<input type='hidden' name='action' value='techCombine'>

			<input type='text' class='techSearch' name='name1' id='currentTechInput'>
			<input type='text' class='techSearch' name='name1' id='leadsToInput'>

			<button type='button' class='btn btn-primary' value='combine' id='combineBtn'>Combine</button>
		</form>


	</div>
	<div class='row-fluid'>
		<p id='currentTech'>Current Tech ID</p>
		<p id='leadsTo'>Leads To ID</p>
	</div>
</div>

<script>
$(document).ready({})
var sourceArr = [
<?php
	$getNames = $db->prepare("SELECT * FROM `gameTechnology`");
	$getNames->execute(array());
	$first = 'yes';
	while($row = $getNames->fetch(PDO::FETCH_ASSOC)){
		
		if($first == 'no'){
			echo ",";
		}else{
			$first = 'no';
		}
		extract($row);
		echo "'$name'";
	}
?>
];
var subjects = ['PHP', 'MySQL', 'SQL', 'PostgreSQL', 'HTML', 'CSS', 'HTML5', 'CSS3', 'JSON'];

	$(".techSearch").typeahead({
		source: sourceArr,
		items:15
	});


	$('#currentTechInput').change(function(){
		var name = $(this).val();
		$.ajax({
			url:'api.php',
			dataType:'json',
			data:{
				'action':'getTechID',
				'name':name
			},
			success:function(data){
				console.log(data);
				$("#currentTechID").remove();
				console.log(data.ID);
				$("#findTechForm").append("<input id='currentTechID' type='hidden' name='currentTech' value='" + data.ID + "'>");
				$("#currentTech").text("Current Tech ID - "+ data.ID);

			},error:function(data){
				console.log(data);
			}
		})
	});

	$('#leadsToInput').change(function(){
		var name = $(this).val();
		$.ajax({
			url:'api.php',
			dataType:'json',
			data:{
				'action':'getTechID',
				'name':name
			},
			success:function(data){
				console.log(data);
				$("#leadsToID").remove();
				console.log(data.ID);
				$("#findTechForm").append("<input id='leadsToID' type='hidden' name='leadsTo' value='" + data.ID + "'>");
				$("#leadsTo").text("Leads To ID - "+ data.ID);

			},error:function(data){
				console.log(data);
			}
		})
	})

	$("#combineBtn").click(function(){
		var current = $("#currentTechID").val();
		var leads = $("#leadsToID").val();
		$.ajax({
			url:'api.php',
			dataType:'json',
			data:{
				'currentTech':current,
				'leadsTo':leads,
				'action':'connectTechs'
			},success:function(data){
				console.log("connected");
				console.log(data);
				$("#leadsToID").remove();
				$("#currentTechID").remove();
				$('input').val("");
			},error:function(data){
				console.log(data);
			}
		})
	})
	</script>
</body>
</html>