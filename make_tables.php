<?php
include "cookout.php";

$ship_classes = array(
"Apis",
"Culicidae",
"Vespula",
"Vespa",
"Hymenoptera",
"Lupus" ,
"Jamaicensis" ,
"Serpentes" ,
"Mississippiensis" ,
"Tigris" ,
"Leo" ,
"Gorilla" ,
"Capensis" ,
"Capra" ,
"Asinus" ,
"Caballus" ,
"Capra-P",
"Primigenius" ,
"Caballus-P",
"Elephantidae" ,
"Tarandus",
"Tarandus-P",
"Macrorhynchos" ,
"Livia" ,
"Africanus" ,
"Strigiformes" ,
"Latrans" ,
"Maritimus" ,
"Macrorhynchos-S"
);

foreach($ship_classes as $value){
	$addCol = $db->prepare("INSERT INTO `ship_classes` (`name`,`code_name`) VALUES (?,?)");
	$code_name = "ships_$value";
	$addCol->execute(array($value,$code_name));
}