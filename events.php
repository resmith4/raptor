<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 'On');
include "objects.php";
include_once "functions.php";

if($_POST[action] == 'addEvent'){
	extract($_POST);
	addEvent($name,$comments,$startDate,$startTime,$endDate,$endTime);
}

?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Events</title>
	<?php include 'linksAndScripts.inc'; ?>
</head>
<body>
<?php include 'templates/navbar.php' ?>
<div class='container-fluid'>
	<div class='row-fluid'>
		<h1 class='center'>Events</h1>
	</div>
	<div class='row-fluid'>
		<div class='span3'>
			<?php include "templates/addEvent.php" ?>
		</div>
		<div class='span9'>
			<?php include "templates/eventsInfo.php";?>
		</div>
	</div>
</div>

<script>

</script>
</body>
</html>