<?php
	include 'potato.php';
	$getUserInfo = $db->prepare("SELECT * FROM `users` WHERE `ID` = ?");
	$getUserInfo->execute(array($_SESSION[UID]));
	$row = $getUserInfo->fetch(PDO::FETCH_ASSOC);
	$getTopGoal = $db->prepare("SELECT * FROM `goals` WHERE `ID` = ?");
	$getTopGoal->execute(array($row[goal1]));
	$goalFinal = array();
	$goalRow = $getTopGoal->fetch(PDO::FETCH_ASSOC);
	array_push($goalFinal,$goalRow[Name]);
	$getTopGoal->execute(array($row[goal2]));
	$goalRow = $getTopGoal->fetch(PDO::FETCH_ASSOC);
	array_push($goalFinal,$goalRow[Name]);
	$getTopGoal->execute(array($row[goal3]));
	$goalRow = $getTopGoal->fetch(PDO::FETCH_ASSOC);
	array_push($goalFinal,$goalRow[Name]);

?>
<div class='navbarDiv span4' id="leftside">
	<div class='userIconBox'>		
		<img src="../img/users/icons/<?php echo $_SESSION[UID].'.jpg' ?>"/>
		<span id='name'><?php echo "$row[firstName]<br>$row[lastName]"?></span>
	</div>
	<p><?php echo $row[username] ?></p>
	<p class='info'><span><?php echo $row[job]?></span> | <?php echo $row[jobDescription]?></p>
	<ul>
		<?php foreach($goalFinal as $value){echo "<li>$value</li>";}; ?>
	</ul>
</div>