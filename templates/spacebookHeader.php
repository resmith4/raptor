<div class='row-fluid'>
	<h1 class='header-logo'>Spacebook</h1>
</div>
<div class='row-fluid'>
	<div class='main-info'>
		<?php include "templates/mainInfo.php" ?>
	</div>
</div>
<div class='row-fluid'>
	<?php include "templates/planetDisplay.php" ?>
</div>
<div class='row-fluid'>				
	<?php include "templates/resourceDisplay.php" ?>
	<div class='center row-fluid'>
		<?php include "templates/mainActionBar.php" ?>
	</div>
</div>