<?php
	$userTasks = getUserTasks($_SESSION[UID]);

		function generateTaskActionButton($taskID){
		echo "<div class='btn-group'>";
		echo "<a href='tasks.php?action=completeTask&ID=$taskID' type='button' class='btn btn-small'>Done!</a><button class='btn btn-small dropdown-toggle' data-toggle='dropdown'><span class='caret'></span></button>";
		echo "<ul class='dropdown-menu'>";
		echo "<li><a href='tasks.php?action=deleteTask&ID=$taskID'>Delete</a></li>";
		echo "<li><a href='tasks.php?action=editTask&ID=$taskID'>Edit</a></li>";
		echo "<li><a href='tasks.php?action=SetParent&ID=$taskID'>Set Parent</a></li>";
		echo "</ul>";
		echo "</div>";
	}

	function echoArray($arr,$parent){
		foreach($arr as $value){
			if($parent == null){$parent = "-none-";}
			if($value[5] == 'no'){
				echo "<tr id='$value[0]' class='taskRow'><td>$value[1]</td><td>$value[2]</td><td>$value[3]</td><td>$value[4]</td><td>$parent</td><td>";
				//breaking for the echo of Task Action button
				generateTAskActionButton($value[0]);
				echo "</td></tr>";
			}
			if($value[6] != null){
				$parent = $value[1];
				echoArray($value[6],$value[1]);
			}
		}
		
	}



?>

<table class='table sortable table-tasks'>
	<thead>
		<tr><th>Name</th><th>Comments</th><th>Urgency</th><th>Importance</th><th>Parent</th><th>Actions</th></tr>
	</thead>
	<tbody>
		<?php @echoArray($userTasks) ?>
	</tbody>
</table>