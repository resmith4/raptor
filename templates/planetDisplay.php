<?php
	$planetArray = getUserPlanets($_SESSION[UID]);
	$location = $vaue[location_galaxy].":".$value[location_system].":".$value[location_planet];
?>

<ul class='thumbnails'>
	<?php 	foreach($planetArray as $value){
				$location = $value[location_galaxy].":".$value[location_system].":".$value[location_planet];
				if($value[ID] == $_SESSION[activePlanet]){$class = "active-planet";}else{$class=null;}

?>
	<li class='<?php echo $class ?> planet-display'>
		<a href='/home.php?action=changePlanet&planetID=<?php echo $value[ID] ?>'>
			<div class='thumbnail'>
				<img src='/img/planets/<?php echo $value[image] ?>'>
				<p><?php echo $value[name] ?></p>
				<p><?php echo $location ?></p>
			</div>
		</a>
	</li>
<?php } //closing up the foreach ?>
</ul>