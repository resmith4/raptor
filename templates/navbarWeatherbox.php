<?php
	include 'potato.php';
	$getLocaleData = $db->prepare("SELECT * FROM `locales` WHERE `ID` IN (SELECT locale FROM `users` WHERE `ID` = ?)");
	$getLocaleData->execute(array($_SESSION[UID])) or die("Weather Check Failed");
	$row = $getLocaleData->fetch(PDO::FETCH_ASSOC);
	$backgroundImg = "style=\"background-image:url('img/locales/$row[weatherImg]');\"";
?>
<div class='navbarDiv span4' id="weatherbox"<?php echo $backgroundImg ?>>
	<p class='city'><?php echo "$row[name], $row[zone0]"?></p>
	<p class='weather'><span>Sunny <?php echo "$row[curHigh]/$row[curLow]" ?></span></p>
	<p>Current Event: Presenting for eGames</p>
	<p>Upcoming Event: Mobile App Development Meeting (1700)</p>
</div>