<?php
  $memberList = generateWorkgroupMemberList($_GET[workgroupID]);
  $teamList = generateWorkgroupTeamList($_GET[workgroupID]);
?>
<div class="tabbable"> <!-- Only required for left/right tabs -->
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tabMembers" data-toggle="tab">Members</a></li>
    <li><a href='#tabTeams' data-toggle='tab'>Teams</a>
    <li><a href="#tabActions" data-toggle="tab">Actions</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tabMembers">
      <?php generateWorkgroupMemberListTable($memberList);
      		 ?>
    </div>
    <div class='tab-pane' id='tabTeams'>
    	<div class='center'><a class='btn btn-large btn-primary btn-margin' href='workgroups.php?action=createTeam&workgroupID=<?php echo $_GET[workgroupID] ?>'>Add Team</a></div>
		<?php displayTeamInfo($teamList); ?>
    </div>
    <div class="tab-pane" id="tabActions">
      <div class='center'>
	      <a class='btn btn-large btn-primary btn-margin' href='workgroups.php?action=addWorkgroupTask&workgroupID=<?php echo $_GET[workgroupID] ?>'>Add Task</a><br>
	      <a class='btn btn-large btn-primary btn-margin' href='workgroups.php?action=addWorkgroupEvent&workgroupID=<?php echo $_GET[workgroupID] ?>'>Add Event</a><br>
	      <a class='btn btn-large btn-primary btn-margin' href='workgroups.php?action=addWorkgroupGoal&workgroupID=<?php echo $_GET[workgroupID] ?>'>Add Goal</a>
	  </div>
    </div>
  </div>
</div>