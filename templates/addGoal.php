<h2>Add Goal</h2>
<form id='addGoalForm' action='goals.php' method='post' class='box-lifted'>
    <input type='hidden' name='action' value='addGoal'>
    <label for='goalName'>Name</label>
	<input type='text' name='name' id='goalName'>
    <label for='date'>Date</label>
    <input type='text' name='date' id='date'>
    <label for='time'>Time</label>
    <input type='text' name='time' id='time'>
    <label for='comments'>Comments</label>
    <textarea name='comments' id='comments'></textarea>
	<input type='submit' value='Submit' class='btn btn-primary'>
</form>
<script>
$('#date').datetimepicker({
    altField: "#time"
});
</script>