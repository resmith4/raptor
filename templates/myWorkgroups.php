<?php
$workgroups = getUserWorkgroups($_SESSION[UID]);
?>
<h2 class='center'>My Workgroups</h2>
<div class='center'>
	<a href="workgroups.php?action=addWorkgroup" class='center btn btn-large btn-primary'>Add New Workgroup</a>
</div>
<div style='padding-top:10px;'>
	<?php generateWorkgroupTable($workgroups); ?>
</div>