<?php
        $ships = get_planet_fleet_ships($_SESSION[activePlanet]);
        $userPlanets = getUserPlanets($_SESSION[UID]);
        if($message != null){
            echo "<div class='game-message ";
            switch($message[type]){
                case "danger":
                    echo "game-message-danger ";
                    break;
            } 
            echo "'>\n";
            echo "<p>$message[message]</p>\n";
            echo "</div>\n";
        }
?>
<p class='planet-overview-header'>Active Fleets</p>
        <?php include 'activeFleets.php'; ?>

        <p class='planet-overview-header'>Assemble Fleet</p>
        <div class='assemble-fleet-screen'>
                <div class='location'>
                        Location:<input class='input-mini' onchange='calculateFleetDistance()' name='destination-galaxy' id="destination-galaxy" type='text'> : <input class='input-mini' onchange='calculateFleetDistance()' name='destination-system' id="destination-system" type='text'> : <input class='input-mini' onchange='calculateFleetDistance()' name='destination-planet' id="destination-planet" type='text'>
                </div>
                <div class='my-planets'>
                        <ul>
                                <?php
                                	foreach($userPlanets as $value){
                                	echo "<li class='user-planet-locator'>$value[name] <span>$value[location_galaxy]:$value[location_system]:$value[location_planet]</span></li>";	
                                	}
                            	?>
                        </ul>
                </div>
                <div class='ships-to-send'>
                        <div class='send-ship-class-box send-fighters'>
                                Fighters
                                <ul class='ship-selection'>
                                        <?php
                                                foreach($ships_fighters as $value){
                                                        $theString = "ships_".$value;
                                                        if($ships[$theString] > 0){
                                                                echo "<li><div class='input-append'><input type='number' class='ship-number-input' name='$theString' id='$theString'  min='0' max='$ships[$theString]'><button class='btn btn-inverse ship-max' value='$ships[$theString]'>M</button'></div> $value</li>";
                                                        }
                                                }
                                        ?>
                                </ul>
                        </div>
                        <div class='send-ship-class-box send-cruisers-light'>
                                Light Crusiers
                                <ul class='ship-selection'>
                                        <?php
                                                foreach($ships_lightCruisers as $value){
                                                        $theString = "ships_".$value;
                                                        if($ships[$theString] > 0){
                                                                echo "<li><div class='input-append'><input type='number' class='ship-number-input' name='$theString' id='$theString'  min='0' max='$ships[$theString]'><button class='btn btn-inverse ship-max' value='$ships[$theString]'>M</button'></div> $value</li>";
                                                        }
                                                }
                                        ?>
                                </ul>
                        </div>
                        <div class='send-ship-class-box send-cruisers-heavy'>
                                Heavy cruisers
                                <ul class='ship-selection'>
                                        <?php
                                                foreach($ships_heavyCruisers as $value){
                                                        $theString = "ships_".$value;
                                                        if($ships[$theString] > 0){
                                                                echo "<li><div class='input-append'><input type='number' class='ship-number-input' name='$theString' id='$theString'  min='0' max='$ships[$theString]'><button class='btn btn-inverse ship-max' value='$ships[$theString]'>M</button'></div> $value</li>";
                                                        }
                                                }
                                        ?>
                                </ul>
                        </div>
                        <div class='send-ship-class-box send-dreadnaught'>
                                Dreadnaughts
                                <ul class='ship-selection'>
                                        <?php
                                                foreach($ships_dreadnaughts as $value){
                                                        $theString = "ships_".$value;
                                                        if($ships[$theString] > 0){
                                                                echo "<li><div class='input-append'><input type='number' class='ship-number-input' name='$theString' id='$theString'  min='0' max='$ships[$theString]'><button class='btn btn-inverse ship-max' value='$ships[$theString]'>M</button'></div> $value</li>";
                                                        }
                                                }
                                        ?>
                                </ul>
                        </div>
                        <div class='send-ship-class-box send-transport'>
                                Transport Ships
                                <ul class='ship-selection'>
                                        <?php
                                                foreach($ships_transport as $value){
                                                        $theString = "ships_".$value;
                                                        if($ships[$theString] > 0){
                                                                echo "<li><div class='input-append'><input type='number' class='ship-number-input' name='$theString' id='$theString'  min='0' max='$ships[$theString]'><button class='btn btn-inverse ship-max' value='$ships[$theString]'>M</button'></div> $value</li>";
                                                        }
                                                }
                                        ?>
                                </ul>
                        </div>
                        <div class='send-ship-class-box send-special'>
                                Special Ships
                                <ul class='ship-selection'>
                                        <?php
                                                foreach($ships_special as $value){
                                                        $theString = "ships_".$value;
                                                        if($ships[$theString] > 0){
                                                                echo "<li><div class='input-append'><input type='number' class='ship-number-input' name='$theString' id='$theString'  min='0' max='$ships[$theString]'><button class='btn btn-inverse ship-max' value='$ships[$theString]'>M</button'></div> $value</li>";
                                                        }
                                                }
                                        ?>
                                </ul>
                        </div>
                </div>
                <div class='fleet-settings'>
                        <div class='center'>
                                Ships <div class='btn-group'><button onclick='noShips()' class='btn btn-inverse'>None</button><button onclick='allShips()' class='btn btn-inverse'>All</button></div>
                        </div>
                        <br>
                        <div class='center'>
                                Orders <div data-toggle="buttons-radio" class='btn-group'><button class='fleet-order btn btn-inverse'>Transport</button><button class='fleet-order btn btn-inverse'>Deploy</button><button class='fleet-order btn btn-inverse'>Harvest</button><button class='fleet-order btn btn-inverse'>Attack</button></div>
                        </div>
                        <div class='row-fluid'>
                                <div class='span5 fleet-resource-select'>
                                        Resources
                                        <div class='input-append'><input onchange='calculateFleetCargoUse()' class='ore-border' name='ore-sent-input' id='ore-sent-input' value='0' type='number'><button onclick="maxResource('available_Ore')" class='btn btn-inverse'>Max</button></div>
                                        <div class='input-append'><input onchange='calculateFleetCargoUse()' class='crystal-border ' name='crystal-sent-input' id='crystal-sent-input' value='0' type='number'><button onclick="maxResource('available_Crystal')" class='btn btn-inverse'>Max</button></div>
                                        <div class='input-append'><input onchange='calculateFleetCargoUse()' class='hydrogen-border ' name='hydrogen-sent-input' id='hydrogen-sent-input' value='0' type='number'><button onclick="maxResource('available_Hydrogen')" class='btn btn-inverse'>Max</button></div>
                                </div>
                                <div class='span7 fleet-assembly-details'>

                                    <p>Distance <span id='fleet-dispatch-distance' class='info-pane-output'>---</span></p>
                                    <p>Time <span id='fleet-dispatch-time' class='info-pane-output'>---</span></p>
                                    <p>Fuel <span id='fleet-dispatch-fuel-consumption' class='info-pane-output'>---</span></p>
                                    <p>Total Cargo Space <span id='fleet-total-cargo-space' class='info-pane-output'>---</span></p>
                                    <p>Available Cargo Space <span id='fleet-available-cargo-space' class='info-pane-output'>---</span></p>
                                </div>
                        </div>
                        <input type='hidden' id='fleetOrders' name='fleetOrders' value="None">
                        <div class='row-fluid'>
                                <button onclick='dispatchFleet()' class='span12 btn btn-primary'>
                                        Dispatch Fleet
                                </button>
                        </div>
                </div>
        </div>

        <script>

                                $(".ship-max").click(function(){
                                        num = $(this).val();
                                        $(this).parent().children("input").val(num);
                                        calculateFleetCargoUse();
                                });

                                $(".user-planet-locator").click(function(){
                                	var location = $(this).children().text();
                                	var locationArray = location.split(":");
                                	var galaxy = locationArray[0];
                                	var system = locationArray[1];
                                	var planet = locationArray[2];
                                	$("#destination-galaxy").val(galaxy);
                                	$("#destination-system").val(system);
                                	$("#destination-planet").val(planet);
                                	calculateFleetDistance();
                                });

                                $(".fleet-order").click(function(){
                                	name = $(this).text();
                                	$("#fleetOrders").val(name);
                                });
                                function noShips(){
                                	$(".ship-number-input").val(0);
                                	calculateFleetSpeed();
                                }

                                function allShips(){
                                	$(".ship-max").click();
                                	calculateFleetSpeed();
                                }
                                
                                $(".ship-number-input").change(function(){
                                	calculateFleetSpeed();
                                });

                                function dispatchFleet(){
                                    $('body').append("<form id='dispatchFleet' action='fleets.php' method='POST'></form>");
                                    form = $("#dispatchFleet");
                                    form.append($("#destination-galaxy"));
                                    form.append($("#destination-system"));
                                    form.append($("#destination-planet"));

                                    shipInputs = $(".ship-number-input");
                                    for(x=0;x < shipInputs.length; x++){
                                        form.append($(shipInputs[x]));
                                    }

                                    form.append($("#ore-sent-input"));
                                    form.append($("#crystal-sent-input"));
                                    form.append($("#hydrogen-sent-input"));

                                    form.append($("#fleetOrders"));

                                    form.append("<input type='hidden' name='action' value='dispatchFleet'>");

                                    form.submit();

                                }
                               
        </script>