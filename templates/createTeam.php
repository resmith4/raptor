<h2>New Team</h2>
<form action='workgroups.php' method='post'>
	<input type='hidden' name='action' value='addTeam'>
	<input type='hidden' name='workgroupID' value='<?php echo $_GET[workgroupID] ?>'>
	<label for='teamName'>Name</label>
	<input type='text' id='teamName' name='name'>
	<label for='description'>Description</label>
	<input type='text' name='description' id='description'>
	<p>You will be added as the administrator of the group, and will be able to add other members later</p>
	<input type='submit' value='Submit' class='btn btn-primary'>
</form>
