<?php 
if($_POST[parentData] != null){
    $parentString = "<input type='hidden' name='parent' value='$_POST[parentData]'>";
}

if($_POST[workgroup] == 'yes'){
    $actionString = "workgroups.php";
    $workgroupInput = "<input type='hidden' name='workgroupID' value='$_GET[workgroupID]'>";

}else{
    $actionString = "tasks.php";
}
?>

<h2>Add Task</h2>
<form id='addTaskForm' action='<?php echo $actionString ?>' method='post' class='box-lifted'>
    <?php echo $workgroupInput; ?>
    <input type='hidden' name='action' value='addTask'>
    <?php echo $parentString ?>
	<label for='taskName'>Name</label>
	<input type='text' name='name' id='taskName'>
	<br><br><label for='urgency'>Urgency</label>
	<input type='text' min='1' max='100' value='50' name='urgency' id='urgency'>
	<div id='uSlider'></div>
	<label for='importance'>Importance</label>
	<input type='text' min='1' max='100' value='50' name='importance' id='importance'>
	<div id='iSlider'></div>
	<label for='comments'>Comments</label>
	<textarea id='comments' name='comments'></textarea>
	<br>
	<input type='submit' value='Submit' class='btn btn-primary'>
</form>
<script>
  $(function() {
    $("#uSlider").slider({
        orientation:'horizontal',
        range:"min",
        min:1,
        max:100,
        value:50,
        slide:function(event,ui){
            $("#urgency").val(ui.value);
        }
    });
    
    $("#iSlider" ).slider({
        orientation:'horizontal',
        range:"min",
        min:1,
        max:100,
        value:50,
        slide:function(event,ui){
            $("#importance").val(ui.value);
        }
    });
});
</script>