<h2>Add Event</h2>
<form id='addGoalForm' action='events.php' method='post' class='box-lifted'>
    <input type='hidden' name='action' value='addEvent'>
    <label for='goalName'>Name</label>
	<input type='text' name='name' id='eventName'>
    <div class='well' style='background:#67E667;border-radius:5px'>
        <h3>Start</h3>
        <label for='startDate'>Date</label>
        <input type='text' name='startDate' id='startDate'>
        <label for='time'>Time</label>
        <input type='text' name='startTime' id='startTime'>
    </div>
    <div class='well' style='background:#FF7373;border-radius:5px'>
        <h3>End</h3>
        <label for='date'>Date</label>
        <input type='text' name='endDate' id='endDate'>
        <label for='time'>Time</label>
        <input type='text' name='endTime' id='endTime'>
    </div>
    <label for='comments'>Comments</label>
    <textarea name='comments' id='comments'></textarea>
	<input type='submit' value='Submit' class='btn btn-primary'>
</form>
<script>
$('#startDate').datetimepicker({
    altField: "#startTime"
});

$('#endDate').datetimepicker({
    altField: "#endTime"
});
</script>