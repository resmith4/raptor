function updateClock(){
	var now = new Date();
	seconds = now.getSeconds();
	seconds = "0" + seconds;
	seconds = seconds.slice(-2);
	time = "<i class='icon-time'></i> " + now.getHours() + ":" + now.getMinutes() + ":" + seconds;
	$("#clock").html(time);
	setTimeout(updateClock,1000);
}

$(document).ready(function(){
	updateClock();
})

function maxResource(input){
	console.log(input);
	switch(input){
		case "available_Ore":
			resourceAmt = $("#available_Ore").val();
			$("#ore-sent-input").val(resourceAmt);
			break;
		case "available_Crystal":
			resourceAmt = $("#available_Crystal").val();
			$("#crystal-sent-input").val(resourceAmt);
			break;
		case "available_Hydrogen":
			resourceAmt = $("#available_Hydrogen").val();
			$("#hydrogen-sent-input").val(resourceAmt);
			break;
   	}
};

function calculateFleetDistance(){
	console.log('calc');
	currentPosString = $("#active_planet_location").val();
	currentPosArray = currentPosString.split(":");
	currentPosGalaxy = currentPosArray[0];
	currentPosSystem = currentPosArray[1];
	currentPosPlanet = currentPosArray[2];

	targetPosGalaxy = $("#destination-galaxy").val();
	targetPosSystem = $("#destination-system").val();
	targetPosPlanet = $("#destination-planet").val();

	if(currentPosGalaxy == targetPosGalaxy){
		//target is in the same Galaxy
		if(currentPosSystem == targetPosSystem){
			//target is in the same system
			if(currentPosPlanet == targetPosPlanet){
				//target is the same planet...weird
				distance = 1;
			}else{
				//target is different planet in same system
				distance = (Math.abs(currentPosPlanet - targetPosPlanet)) * 10;
			}
		}else{
			//target is a different system in the same galaxy
			distance = (Math.abs(currentPosSystem - targetPosSystem)) * 1000;
		}
	}else{
		//target is a different galaxy. Have fun with that one
		distance = (Math.abs(currentPosGalaxy - targetPosGalaxy)) * 1000000;
	}
	$("#fleet-dispatch-distance").text(distance);
	calculateFleetSpeed();
}

function calculateFleetSpeed(){
	finalOutput = new Array();
	shipInputs = $(".ship-number-input");
	for(x=0;x < shipInputs.length; x++){
		output = new Array();
		output[0] = $(shipInputs[x]).attr('id');
		output[1] = $(shipInputs[x]).val();
		if(output[1] > 0){
			finalOutput.push(output);	
		}
	}
	distance = $("#fleet-dispatch-distance").text();
	$.ajax({
		url:'api.php',
		dataType:'json',
		data:{
			'action':'getFleetSpeed',
			'distance':distance,
			'ships':finalOutput
		},
		success:function(data){
			console.log(data);
			hrs = data/60;
			hours = Math.floor(hrs);
			minutes = data%60;
			minutes = Math.floor(minutes);
			$("#fleet-dispatch-time").text(hours + ":" + minutes + ":00");
			calculateFleetFuelConsumption();
			calculateFleetCargoCapacity();
		},
		error:function(data){
			console.log(data);
		}
	})
}

function calculateFleetFuelConsumption(){
	finalOutput = new Array();
	shipInputs = $(".ship-number-input");
	for(x=0;x < shipInputs.length; x++){
		output = new Array();
		output[0] = $(shipInputs[x]).attr('id');
		output[1] = $(shipInputs[x]).val();
		if(output[1] > 0){
			finalOutput.push(output);	
		}
		
	}
	distance = $("#fleet-dispatch-distance").text();
	$.ajax({
		url:'api.php',
		dataType:'json',
		data:{
			'action':'getFleetFuelConsumption',
			'distance':distance,
			'ships':finalOutput
		},
		success:function(data){
			console.log(data);
			$("#fleet-dispatch-fuel-consumption").text(data);
		},
		error:function(data){
			console.log(data);
		}
	})
}

function calculateFleetCargoCapacity(){
	finalOutput = new Array();
	shipInputs = $(".ship-number-input");
	for(x=0;x < shipInputs.length; x++){
		output = new Array();
		output[0] = $(shipInputs[x]).attr('id');
		output[1] = $(shipInputs[x]).val();
		if(output[1] > 0){
			finalOutput.push(output);	
		}
		
	}
	distance = $("#fleet-dispatch-distance").text();
	$.ajax({
		url:'api.php',
		dataType:'json',
		data:{
			'action':'getFleetCargoCapacity',
			'ships':finalOutput
		},
		success:function(data){
			console.log(data);
			$("#fleet-total-cargo-space").text(data);
			calculateFleetCargoUse();
		},
		error:function(data){
			console.log(data);
		}
	})
}

function calculateFleetCargoUse(){
	var cargoUsed = parseInt($("#ore-sent-input").val()) + parseInt($("#crystal-sent-input").val()) + parseInt($("#hydrogen-sent-input").val());
	var cargoSpace = $("#fleet-total-cargo-space").text();
	var availableSpace = cargoSpace - cargoUsed;
	$("#fleet-available-cargo-space").text(availableSpace);
}