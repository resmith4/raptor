<!DOCTYPE HTML>
<HTML>
	<Head>
		<title>Project Raptor | Home</title>
		<?php include "linksAndScripts.inc" ?>
	</head>
	<body>
		<?php include 'templates/navbar.php';?>
		<div class='container-fluid' style='margin-top:10px'>
			<?php if($_POST[action] == 'superBar'){include 'templates/processSuperBar.php';} ?>
			<div class="row-fluid">
				<div class='span4'>
					<h1>Planner</h1>
					This is the section for planning day-to-day
					<div class="tabbable"> <!-- Only required for left/right tabs -->
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1" data-toggle="tab">Today</a></li>
							<li><a href="#tab2" data-toggle="tab">Week</a></li>
							<li><a data-toggle='tab'>Month</a></li>
							<li><a data-toggle='tab'>Year</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab1">
								<p>I'm in Section 1.</p>
						</div>
						<div class="tab-pane" id="tab2">
							<p>Howdy, I'm in Section 2.</p>
						</div>

						</div>
					</div>
				</div>
				<div class='span4'>
					<h1>Goals</h1>
				</div>
				<div class='span4'>
					<h1>Achivements</h1>
					This is the section for managing your achivements
				</div>
			</div>
		</div>

	</body>
