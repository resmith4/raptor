<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 'On');
include "objects.php";
include_once "functions.php";

if($_POST[action] == 'addTask'){
	extract($_POST);
	addTask($name,$comments,$urgency,$importance,$_SESSION[UID],$parent);
}

switch($_GET[action]){
	case "completeTask":
		$completeTask = $db->prepare("UPDATE `tasks` SET `complete` = 'yes' WHERE `ID` = ?");
		$completeTask->execute(array($_GET[ID]));
		break;
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Tasks</title>
	<?php include 'linksAndScripts.inc'; ?>
</head>
<body>
<?php include 'templates/navbar.php' ?>
<div class='container-fluid'>
	<div class='row-fluid'>
		<h1 class='center'>Tasks</h1>
	</div>
	<div class='row-fluid'>
		<div class='span3'>
			<?php include "templates/addTask.php" ?>
		</div>
		<div class='span9'>
			<?php include "templates/taskInfo.php";?>
		</div>
	</div>
</div>

<script>
	$(".taskRow").click(function(){
		console.log("potato");
		name = $(this).children().first().text();
		rowID = $(this).attr('id');
		appendHTML = "<div class='addParentDiv'><br><input type='hidden' name='parent' value='"+rowID+"'><p class='well'>New task will be have "+name+" as a parent task<button onclick='cancelParent()' type='button' class='pull-right btn btn-small btn-danger'>Cancel</button></p></div>";
		cancelParent();
		$("#addTaskForm").append(appendHTML);
	});
	function cancelParent(){
		$(".addParentDiv").remove();
	}
</script>
</body>
</html>