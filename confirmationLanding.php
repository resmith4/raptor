<?php include_once "templates/bootTemplate.php";
	if($_POST[action] == "changePass"){
		if($_POST[password] == $_POST[rePassword]){
			include 'potato.php';
			$changePass = $db->prepare("UPDATE `users` SET `password` = ? , `confirmed` = 'yes' WHERE `ID` = ?");
			$newPass = md5($_POST[password]); 
			$changePass->execute(array($newPass,$_SESSION[UID]))or die(var_dump($db->errorInfo()));
			$_POST[message] = "Your password has been updated";
			include "indexLoggedIn.php";die();
		}else{
			$_POST[message] = "The two password fields do not match.";
		}
	}
 ?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Confirmation Page</title>
	<?php include 'linksAndScripts.inc' ?>
</head>
<body>
	<div class='hero-unit'>
		<h1>Confirmed!</h1>
		<p>You are now confirmed. Please enter a permanent password.</p>
		<p><?php echo $_POST[message] ?></p>
		<form action='confirmationLanding.php' method='POST'>
			<input type='hidden' name='action' value='changePass'>	
			<label for='password'>Password</label>
			<input id='password' type='password' name='password'>
			<label for='rePassword'>Password (again)</label>
			<div class='input-append'>
			<input id='rePassword' type='Password' name="rePassword">
			<input type='submit' value='Submit' class='btn btn-primary'>
		</div>
</body>
</html>