<?php

include "cookout.php";
include "functions.php";

if ($_GET[action] == "getTechID"){
	$getTechID = $db->prepare("SELECT * FROM `gameTechnology` WHERE `name` = ? LIMIT 1");
	$getTechID->execute(array($_GET[name])) or die("search");
	$row = $getTechID->fetch(PDO::FETCH_ASSOC);
	echo json_encode($row);
}

if($_GET[action] == "connectTechs"){
	include 'cookout.php';
	extract($_GET);
	$currentTech;
	$leadsTo;
	$getInfo = $db->prepare("SELECT * FROM `gameTechnlogyRequirements` WHERE `currentTech` = ? AND `leadsTo` = ?");
	$getInfo->execute(array($currentTech,$leadsTo))or die(var_dump($db->errorInfo()));
	$num = $getInfo->rowCount();
	if($num == 0){
		$insertInfo = $db->prepare("INSERT INTO `gameTechnlogyRequirements` (`currentTech`,`leadsTo`) VALUES (?,?)");
		$insertInfo->execute(array($currentTech,$leadsTo)) or die(var_dump($db->errorInfo()));
		echo json_encode(array("completed"));die();
	}else{
		echo json_encode(array("There are already $num lines of this connection")); 
	}
}

if($_GET[action] == "getFleetSpeed"){
	$travelTime = getFleetSpeed($_GET[ships],$_GET[distance]);
	echo json_encode($travelTime);

}

if($_GET[action] == "getFleetFuelConsumption"){
	$fuelConsumption = getFleetFuelConsumption($_GET[ships],$_GET[distance]);
	echo json_encode($fuelConsumption);

}

if($_GET[action] == "getFleetCargoCapacity"){
	$cargoCapacity = getFleetCargoSpace($_GET[ships]);
	echo json_encode($cargoCapacity);
}

?>